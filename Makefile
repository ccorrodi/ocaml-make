SOURCEDIRS = src /tmp/src
OBJDIR = obj
CAMLDIR =
NATIVECAML =
DEBUG = 1
COMPILEFLAGS = -I $(OBJDIR)
COMMAND = echo

CAMLLEX = ocamllex
CAMLYACC= ocamlyacc -v

MLLS = lexer.mll
MLYS = parser.mly

ifdef DEBUG
  COMPILEFLAGS += -g -annot
endif

ifdef NATIVECAML
  CAMLC = $(CAMLDIR)ocamlopt $(COMPILEFLAGS)
  CAMLLINK = $(CAMLDIR)ocamlopt $(LINKFLAGS)
  CMO = cmx
  MOVEAFTERCAMLC = cmi cmx o
else
  CAMLC = $(CAMLDIR)ocamlc $(COMPILEFLAGS)
  CAMLLINK = $(CAMLDIR)ocamlc $(LINKFLAGS)
  CMO = cmo
  MOVEAFTERCAMLC = cmi cmo
endif

vpath %.mll $(SOURCEDIRS)
vpath %.mly $(SOURCEDIRS)
vpath %.ml  $(SOURCEDIRS) $(OBJDIR)
vpath %.mli $(SOURCEDIRS) $(OBJDIR)
vpath %.c   $(SOURCEDIRS)

PROJECT_EXECUTABLE = $(OBJDIR)/main
PROJECT_MODULES = parser main

all: $(PROJECT_EXECUTABLE)

$(PROJECT_EXECUTABLE) : $(PROJECT_MODULES:%=$(OBJDIR)/%.$(CMO)) \
                        $(PROJECT_CMODULES:%=$(OBJDIR)/%.$(CMC))
	$(AT)$(CAMLLINK) -verbose -o $@ \
                    $^

MLL_LYS:= $(MLLS:%.mll=$(OBJDIR)/%.ml)  \
          $(MLYS:%.mly=$(OBJDIR)/%.ml) $(MLYS:%.mly=$(OBJDIR)/%.mli)

.SECONDARY : $(MLL_LYS)

$(OBJDIR)/%.ml: %.mll
	$(CAMLLEX) $<
	$(AT)mv -f $(basename $<).ml $(OBJDIR)/
	$(ECHO)if test -f $(basename $<).mli ;then \
	  $(COMMAND) cp -f $(basename $<).mli $(OBJDIR)/; \
	  cp -f $(basename $<).mli $(OBJDIR)/ \
        ;fi

$(OBJDIR)/%.ml $(OBJDIR)/%.mli: %.mly
	$(CAMLYACC) $(CAMLYACCFLAGS) $<
	$(CAMLC) $(basename $<).mli
	$(AT)mv -f $(basename $<).ml $(basename $<).mli $(basename $<).cmi $(OBJDIR)/

$(OBJDIR)/%.cmi: %.mli
	@$(NARRATIVE) Compiling interface $<
	$(AT)$(CAMLC) -c $<
	$(ECHO)if test $(OBJDIR) != $(<D) ;then \
                   $(COMMAND) mv -f $(basename $<).cmi $(OBJDIR)/; \
                   mv -f $(basename $<).cmi $(OBJDIR)/ \
        ;fi

$(OBJDIR)/%.$(CMO):$ %.ml
	@$(COMMAND) $(OCAMLC) -c $<
	$(CAMLC) -c $< ; res=$$?; \
	   if test $(OBJDIR) != $(<D) ;then \
              for ext in $(MOVEAFTERCAMLC); do \
                if test -f $(basename $<).$$ext ;then \
                  $(COMMAND) mv -f $(basename $<).$$ext $(OBJDIR)/; \
                  mv -f $(basename $<).$$ext $(OBJDIR)/; \
                fi; \
              done; \
           fi; exit $$res

clean: cleancaml

.PHONY: cleancaml
cleancaml:
	-for srcdir in $(SOURCEDIRS); do \
                rm -f $$srcdir/*.annot; \
                rm -f $$srcdir/*.output; \
	done;
	-rm -f $(OBJDIR)/*.cmi
	-rm -f $(OBJDIR)/*.cmo
	-rm -f $(OBJDIR)/*.cmx
	-rm -f $(OBJDIR)/*.o
	-rm -f $(OBJDIR)/*.a
	-rm -f $(OBJDIR)/*.mli
	-rm -f $(OBJDIR)/*.ml
	-rm -f $(DEPENDDIR)/*.d $(DEPENDDIR)/*.di
	-rm -f $(MLLS:%.mll=$(OBJDIR)/%.ml) \
               $(MLLS:%.mll=$(OBJDIR)/%.mli) \
               $(MLYS:%.mly=$(OBJDIR)/%.ml) \
               $(MLYS:%.mly=$(OBJDIR)/%.mli)
