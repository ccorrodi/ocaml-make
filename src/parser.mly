%{
%}

%token EOF

%start input
%type <unit> input

%%

input:
  | EOF
    { () }
